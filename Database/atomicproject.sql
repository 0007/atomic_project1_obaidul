-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2016 at 08:17 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday_tb`
--

CREATE TABLE IF NOT EXISTS `birthday_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `DoB` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `birthday_tb`
--

INSERT INTO `birthday_tb` (`id`, `name`, `DoB`) VALUES
(5, 'Obaidul Hoque Nirzon', '1985-08-16'),
(6, 'Shamim', '2016-01-03');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`) VALUES
(5, 'Computer Fundamental', 'nirzon nnnnn');

-- --------------------------------------------------------

--
-- Table structure for table `city_tb`
--

CREATE TABLE IF NOT EXISTS `city_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `city_tb`
--

INSERT INTO `city_tb` (`id`, `name`, `city`) VALUES
(21, 'Nirzon', 'Dhaka'),
(22, 'Sunjid', 'Feni'),
(23, 'vvvvvvv', 'Khulna');

-- --------------------------------------------------------

--
-- Table structure for table `email_tb`
--

CREATE TABLE IF NOT EXISTS `email_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `email_tb`
--

INSERT INTO `email_tb` (`id`, `name`, `email_id`) VALUES
(16, 'Nirzon', 'obaied85@gmil.com'),
(17, 'Md. Obaidul hoque', 'g@d');

-- --------------------------------------------------------

--
-- Table structure for table `gender_tb`
--

CREATE TABLE IF NOT EXISTS `gender_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `gender_tb`
--

INSERT INTO `gender_tb` (`id`, `name`, `gender`) VALUES
(30, 'Md. Obaidul hoque', 'Female'),
(33, 'Nirzon', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `multicheck_tb`
--

CREATE TABLE IF NOT EXISTS `multicheck_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mcheck` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `multicheck_tb`
--

INSERT INTO `multicheck_tb` (`id`, `name`, `mcheck`) VALUES
(1, 'Md. Obaidul hoque', 'a:3:{i:0;s:7:"Cricket";i:1;s:8:"Football";i:2;s:7:"Fishing";}'),
(2, 'jahid', 'a:2:{i:0;s:7:"Cricket";i:1;s:7:"Fishing";}'),
(3, 'Nirzon', 'a:3:{i:0;s:7:"Cricket";i:1;s:8:"Football";i:2;s:7:"Fishing";}'),
(4, 'Kamal', 'a:2:{i:0;s:7:"Cricket";i:1;s:8:"Football";}'),
(5, 'Md. Obaidul hoque', 'a:3:{i:0;s:7:"Cricket";i:1;s:8:"Football";i:2;s:7:"Fishing";}');

-- --------------------------------------------------------

--
-- Table structure for table `orgsummary_tb`
--

CREATE TABLE IF NOT EXISTS `orgsummary_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `orgsummary_tb`
--

INSERT INTO `orgsummary_tb` (`id`, `name`, `summary`) VALUES
(10, 'Md. Obaidul hoque', 'MAIT UK'),
(11, 'Md. Obaidul hoque', 'BITM Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture_tb`
--

CREATE TABLE IF NOT EXISTS `profilepicture_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `picfile` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `profilepicture_tb`
--

INSERT INTO `profilepicture_tb` (`id`, `name`, `picfile`) VALUES
(7, 'Md. Obaidul hoque', 'img/obaidul Pasport.jpg'),
(8, 'Nirzon', 'img/obaidul Pasport 0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `singlecheck_tb`
--

CREATE TABLE IF NOT EXISTS `singlecheck_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `onechoose` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `singlecheck_tb`
--

INSERT INTO `singlecheck_tb` (`id`, `name`, `onechoose`) VALUES
(54, 'Obaidul Hoque', 'BITM'),
(61, 'Obaidul Hoque', 'BITM'),
(62, 'Obaidul Hoque', 'BITM'),
(63, 'Obaidul Hoque', 'BITM');
