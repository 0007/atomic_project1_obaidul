<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Multicheck\Multicheck;

$obj=new Multicheck();
$checks=$obj->index();
?>

<html>
    <head>
        <title>Multi Check</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link href="css/style.css" type="text/css" rel="stylesheet">-->
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    
    <body>
        
        <div class="main_continar">
            <div class="title"><h1>Multi Check</h1></div> 
            
            <div class="nav">
               <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
               <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
               <a href="create.php"><strong>ADD</strong></a>
            </div>
            
            <div class="info_containar">
                 <div class="info">
                    <span id="utility">Download as PDF XL | Filter |</span>
                    <span>Search |</span>
                    <span>View
                        <select>
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                            <option>50</option>
                        </select>
                    </span>
                </div>
            </div>
            
            
        <div class="table_size">             
            <center>
                <table border="1">
                        <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Name</th>
                                <th>Hobby</th>
                                <th>Action</th>
                            </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                    foreach($checks as $check):
                    ?>
                    <tr>
                       <td><?php echo "(".$i++.")"?></td>               
                       <td><?php echo "&nbsp".$check['name'];?></td>
                       
                       <td>&nbsp;<?php if(unserialize($check['mcheck'])) { echo implode(" ",unserialize($check['mcheck']));}?></td>
                       
                    <!--  <td>&nbsp;<?php // if(!empty(unserialize($check['mcheck']))) { echo implode(", ", unserialize($check['mcheck'])); }?></td>  -->
                       <td class="tablerightcol"> 
                           <a href="view.php?id=<?php echo $check['id']; ?>">View</a>
                           <a href="edit.php?id=<?php echo $check['id']; ?>">Edit</a>
                            <a href="delete.php?id=<?php echo $check['id']; ?>">Delete </a>
                                               
                           | Trash/Recover | email to Friend </td>
                   </tr>

                    <?php 
                    endforeach;
                    ?>
                </tbody>
            </table>

            </center>
                
                <div class="table_footer">
                    <a class="link" href="create.php">Add</a>
                </div>
            <div class="containfooter"><span> <p>Previous  1 | 2 | 3 | ...... Next </p></span></div>
            
            </div>
            
            <div class="footer">Md.Obaidul Hoque | SEIP: 108098 | PHP-13</div>
        </div>
        
    </body>
    
</html>