<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Multicheck\Multicheck;
$id=$_GET['id'];
$obj=new Multicheck();
$checks=$obj->Select($id);
?>
<html>
    <head>
        <title>View</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
    <div class="main_continar">
        <div class="title"><h1>View Multi Check</h1></div> 
        <div class="nav">
               <a href="../../../../index.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                 <a href="index.php"><strong>LIST</strong></a>&nbsp;&nbsp;
                 <a href="create.php"><strong>ADD</strong></a>&nbsp;&nbsp;
        </div>  

        <div class="bookcreate_body ">
                <div class="booktitle_create"> 
                    <table class="noborder">
                        <tr><td colspan="3"><b>Multi Check</b></td></tr>
                        <tr>   
                            <?php foreach($checks as $check):?>   
                            <td>Id</td> <td>:</td> <td>&nbsp;<?php echo $check['id'];?></td>
                        </tr>
                        
                        <tr><td>Name</td>  <td>:</td>  <td>&nbsp;<?php echo $check['name'];?></td></tr>
                        <tr><td>Hobby</td> <td>:</td>  <td>&nbsp;<?php echo implode(", ", unserialize($check['mcheck']));?></td></tr>
                        <?php  endforeach; ?>   
                    </table>    
                 </div>
            </div> 
        
        
        
        <div class="footer">
            <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
        </div>

</body>
</html>
