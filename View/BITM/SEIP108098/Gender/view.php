<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Gender\Gender;
$id=$_GET['id'];
$obj=new Gender();
$gender=$obj->Select($id);
?>
<html>
    <head>
        <title>View</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
    <div class="main_continar">
        <div class="title"><h1>View Gender</h1></div> 
        <div class="nav">
               <a href="../../../../index.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                 <a href="index.php"><strong>LIST</strong></a>&nbsp;&nbsp;
                 <a href="create.php"><strong>ADD</strong></a>&nbsp;&nbsp;
        </div>  

        <div class="bookcreate_body ">
                <div class="booktitle_create"> 
                    <table class="noborder">
                        <tr><td colspan="3"><b>View of Book Title</b></td></tr>
                        <tr>   
                            <?php foreach($gender as $gender):?>   
                            <td>Id</td> <td>:</td> <td>&nbsp;<?php echo $gender['id'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Book Title</td> <td>:</td> <td>&nbsp;<?php echo $gender['name'];?></td></tr>
                        <tr><td>Author</td> <td>:</td> <td>&nbsp;<?php echo $gender['gender'];?></td></tr>
                        <?php  endforeach; ?>   
                    </table>    
                 </div>
            </div> 
        
        
        
        <div class="footer">
            <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
        </div>

</body>
</html>
