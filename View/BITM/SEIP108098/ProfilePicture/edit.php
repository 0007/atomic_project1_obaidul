<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\ProfilePicture\ProfilePicture;

$id=$_GET['id'];
$obj=new ProfilePicture();
$files=$obj->Select($id);
?>

<html>
    <head>
        <title>Edit Profile Picture</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="main_continar_email_edit">
            <div class="email_edit_title"><h1>Profile Picture</h1></div> 
            <div class="email_edit_nev">
                <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                 <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                <a href="index.php"><strong>VIEW</strong></a>
            </div> 
            
            <div class="email_edit_body">
                <center>
                    <form action="update.php" method="post" enctype="multipart/form-data">  
                        <table class="email_edit_table">
                            <?php
                            foreach($files as $file):
                            ?>
                            <tr><td>Update Name:</td></tr>
                            <tr><td><input type="text" name="name" autofocus="this" value="<?php echo $file['name'];?>"/> <input name="id" autofocus="this" type="hidden" value="<?php echo $file['id'];?>"/></td></tr>
                                
                            <tr><td><img src="<?php echo $file['picfile'];?>" width="80" height="100"></td></tr>
                                <tr><td>Change Picture: </td></tr>
                                <tr><td><input type="file" name="picfile" value=""></td></tr>
                                <input name="oldimg" type="hidden" value="<?php echo $file['picfile'];?>"/>
                                
                            <?php 
                            endforeach;
                            ?>
                                <tr><td><button class="sbmtbtn" type="submit">Update</button></td></tr>
                        </table>
                    </form>
                </center>
            </div>
            
            <div class="footer">
                <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
            </div>
        </div>   
</body>
</html>
