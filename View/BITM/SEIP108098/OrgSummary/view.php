<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Orgsummary\Orgsummary;
$id=$_GET['id'];
$summary=new Orgsummary();
$summary=$summary->Select($id);
?>
<html>
    <head>
        <title>View</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
    <div class="main_continar">
        <div class="title"><h1>View Organization Summary</h1></div> 
        <div class="nav">
               <a href="../../../../index.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                 <a href="index.php"><strong>LIST</strong></a>&nbsp;&nbsp;
                 <a href="create.php"><strong>ADD</strong></a>&nbsp;&nbsp;
        </div>  

        <div class="bookcreate_body ">
                <div class="booktitle_create"> 
                    <table class="noborder">
                        <tr><td colspan="3"><b>View of Orgsummary</b></td></tr>
                        <tr>   
                            <?php foreach($summary as $summary):?>   
                            <td>Id</td> <td>:</td> <td>&nbsp;<?php echo $summary['id'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Name</td> <td>:</td> <td>&nbsp;<?php echo $summary['name'];?></td></tr>
                        <tr><td>Organization Summary</td> <td>:</td> <td>&nbsp;<?php echo $summary['summary'];?></td></tr>
                        <?php  endforeach; ?>   
                    </table>    
                 </div>
            </div> 
        
        
        
        <div class="footer">
            <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
        </div>

</body>
</html>
