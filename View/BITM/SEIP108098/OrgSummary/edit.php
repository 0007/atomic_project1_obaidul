<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Orgsummary\Orgsummary;
$id=$_GET['id'];
$summary=new Orgsummary();
$summary=$summary->Select($id);

//print_r($book);
?>
<html>
    <head>
        <title>Edit Orgsummary</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="main_continar_email_edit">
            <div class="email_edit_title"><h1>Organization Summary List</h1></div> 
            <div class="email_edit_nev">
                <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                 <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                <a href="index.php"><strong>VIEW</strong></a>
            </div> 
            
            <div class="email_edit_body">
                <center>
                    <form action="update.php" method="post">  
                        <table class="email_edit_table">
                            <?php
                            foreach($summary as $summary):
                            ?>
                            <tr><td>Name</td></tr>
                                <tr><td><input name="name" autofocus="this" value="<?php echo $summary['name'];?>"/> <input name="id" autofocus="this" type="hidden" value="<?php echo $summary['id'];?>"/></td></tr>
                                
                                <tr><td>Organization Summary</td></tr>
                                <tr><td><textarea rows="4" cols="55" name="summary" ><?php echo $summary['summary'];?></textarea></td></tr>
                            
                            <?php 
                            endforeach;
                            ?>
                                <tr><td><button class="sbmtbtn" type="submit">Update</button></td></tr>
                        </table>
                    </form>
                </center>
            </div>
            
            <div class="footer">
                <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
            </div>
        </div>   
</body>
</html>
