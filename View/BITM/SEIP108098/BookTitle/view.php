<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Book\Book;
$id=$_GET['id'];
$book=new Book();
$book=$book->Select($id);
?>

<html>
    <head>
        <title>View</title>
        <meta charset="UTF-8">        
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
       <div class="main_continar">
            <div class="title"><h1>VIEW BOOK TITLE</h1></div> 
            
            <div class="nav">
               <a class="navlink" href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
               <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
               <a href="index.php"><strong>LIST</strong></a>&nbsp;&nbsp;
               <a href="create.php"><strong>ADD</strong></a>
            </div>
           
            <div class="bookcreate_body ">
                <div class="booktitle_create"> 
                    <table class="noborder">
                        <tr><td colspan="3"><b>View of Book Title</b></td></tr>
                        <tr>   
                            <?php foreach($book as $books): ?>   
                            <td>Id</td> <td>:</td> <td>&nbsp;<?php echo $books['id'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Book Title</td> <td>:</td> <td>&nbsp;<?php echo $books['title']?></td></tr>
                        <tr><td>Author</td> <td>:</td> <td>&nbsp;<?php echo $books['author']?></td></tr>
                        <?php  endforeach; ?>   
                    </table>    
                 </div>
            </div>         
    <div class="footer">Md.Obaidul Hoque | SEIP: 108098 | PHP-13</div> 
    </div>      
</body>
</html>
