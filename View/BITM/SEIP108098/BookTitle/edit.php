<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Book\Book;
$id=$_GET['id'];
$book=new Book();
$book=$book->Select($id);
?>

<html>
    <head>
        <title>Create Book Title</title>
        <meta charset="UTF-8">
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    
    <body>
        
        <div class="main_continar">
            <div class="title"><h1>LIST OF BOOKS LITLE</h1></div> 
            
            <div class="nav">
               <a class="navlink" href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
               <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
               <a href="index.php"><strong>VIEW</strong></a>
            </div>
           
            <div class="bookcreate_body">
                <div class="booktitle_create">
                    <form action="update.php" method="post">  
                    <?php
                    foreach($book as $books):
                    ?>
                    <span>Book Title</span><br> 
                    <input name="title" autofocus="this" value="<?php echo $books['title'];?>"/> 
                    <input name="id" autofocus="this" type="hidden" value="<?php echo $books['id'];?>"/><br><br>
                    <span>Author </span><br><input name="author" value="<?php echo $books['author'];?>"><br><br>
                    <?php 
                    endforeach;
                    ?>
                    <span><button  type="submit" class="sbmtbtn">Update</button></span>
                    </form>
                </div>  
            </div>
           
            <div class="footer">Md.Obaidul Hoque | SEIP: 108098 | PHP-13</div>
        </div>
        
    </body>
</html>
