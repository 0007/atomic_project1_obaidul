<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Book\Book;

$book=new Book();
$book=$book->index();
?>


<html>
    <head>
        <title>List Book Title</title>
        <meta charset="UTF-8">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    
    <body>
        
        <div class="main_continar">
            <div class="title"><h1>LIST OF BOOKS LITLE</h1></div> 
            
            <div class="nav">
               <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
               <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
               <a href="create.php"><strong>ADD</strong></a>
            </div>
            
            <div class="info_containar">
                 <div class="info">
                    <span id="utility">Download as PDF XL | Filter |</span>
                    <span>Search |</span>
                    <span>View
                        <select>
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                            <option>50</option>
                        </select>
                    </span>
                </div>
            </div>
            
            
            <div class="table_size">             
                <center>
                    <table  border="1">
                        <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Book Title</th>
                                <th>Author</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i=1;
                            foreach($book as $books):
                            ?>
                            <tr>
                               <td><?php echo "(".$i++.")"?></td>               
                               <td><?php echo "&nbsp".$books['title'];?></td>
                               <td><?php echo "&nbsp".$books['author'];?></td>
                               <td class="tablerightcol"> 
                                   <a href="view.php?id=<?php echo $books['id']; ?>">View</a>
                                   <a href="edit.php?id=<?php echo $books['id']; ?>"> | Edit | </a><a href="delete.php?id=<?php echo $books['id']; ?>">Delete </a> | Trash/Recover | Email to Friend 
                               </td>
                           </tr>

                            <?php 
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </center>
                
                
                
                <div class="table_footer">
                    <a class="link" href="create.php">ADD BOOK TITLE</a>
                </div>
                
                 <div class="containfooter"><span> <p>Previous  1 | 2 | 3 | ...... Next </p></span></div>
                 
            </div>
            
            <div class="footer">Md.Obaidul Hoque | SEIP: 108098 | PHP-13</div>
        </div>
        
    </body>
    
</html>