
<html>
    <head>
        <title>Create City</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="main_continar_email_edit">
            <div class="email_edit_title"><h1>CITY</h1></div> 
            <div class="email_edit_nev">
                <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                 <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                <a href="index.php"><strong>LIST</strong></a>
            </div> 
            
            <div class="email_edit_body">
                <center>
                   <form action="store.php" method="post">  
                        <table class="email_edit_table">
                            <tr><td>Name:</td></tr>
                            <tr><td><input type="text" name="name" autofocus="this" placeholder="Enter Name"/></td></tr>
                                
                            <tr><td>City:</td></tr>
                            <tr><td>
                                    <select name="city">
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Khulna">Khulna</option>
                                        <option value="Jessore">Jessore</option>
                                        <option value="Feni">Feni</option>
                                        <option value="Chittagong">Chittagong</option>
                                     </select>   
                                    
                            </td></tr>
                            <tr><td><button class="sbmtbtn" type="submit">Save</button></td></tr>
                        </table>
                    </form>
                </center>
            </div>
            
            <div class="footer">
                <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
            </div>
        </div>   
</body>
</html>
