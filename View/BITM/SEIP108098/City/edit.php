<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\City\City;
$id=$_GET['id'];
$obj=new City();
$city=$obj->Select($id);

//print_r($book);
?>
<html>
    <head>
        <title>Edit City</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="main_continar_email_edit">
            <div class="email_edit_title"><h1>EMAIL LIST</h1></div> 
            <div class="email_edit_nev">
                <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                 <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                <a href="index.php"><strong>VIEW</strong></a>
            </div> 
            
            <div class="email_edit_body">
                <center>
                    <form action="update.php" method="post">  
                        <table class="email_edit_table">
                            <?php
                            foreach($city as $city):
                            ?>
                            <tr><td>Edit City</td></tr>
                                <tr><td><input name="name" autofocus="this" value="<?php echo $city['name'];?>"/> <input name="id" autofocus="this" type="hidden" value="<?php echo $city['id'];?>"/></td></tr>
                                
                                <tr><td>City</td></tr>
                                
                                <tr><td>
                                    <select name="city">
                                        <option <?php if($city['city']=="Dhaka") echo "selected"?> value="Dhaka">Dhaka</option>
                                        <option <?php if($city['city']=="Khulna") echo "selected"?> value="Khulna">Khulna</option>
                                        <option <?php if($city['city']=="Jessore") echo "selected"?> value="Jessore">Jessore</option>
                                        <option <?php if($city['city']=="Feni") echo "selected"?> value="Feni">Feni</option>
                                        <option <?php if($city['city']=="Chittagong") echo "selected"?> value="Chittagong">Chittagong</option>
                                    </select>   
                            </td></tr>
                            <?php 
                            endforeach;
                            ?>
                                <tr><td><button class="sbmtbtn" type="submit">Update</button></td></tr>
                        </table>
                    </form>
                </center>
            </div>
            
            <div class="footer">
                <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
            </div>
        </div>   
</body>
</html>
