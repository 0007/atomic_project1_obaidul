<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\SingleCheckbox\SingleCheckbox;
$id=$_GET['id'];
$obj=new SingleCheckbox();
$schecks=$obj->Select($id);

//print_r($book);
?>
<html>
    <head>
        <title>Edit Single Check</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="main_continar_email_edit">
            <div class="email_edit_title"><h1>Single Check</h1></div> 
            <div class="email_edit_nev">
                <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                 <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                <a href="index.php"><strong>VIEW</strong></a>
            </div> 
            
            <div class="email_edit_body">              
                <center>
                   <form action="update.php" method="post">  
                        <table class="email_edit_table">
                            <?php
                            foreach($schecks as $scheck):
                            ?>
                            <tr><td>Name:</td></tr>
                            <tr><td><input type="text" name="name" autofocus="this" value="<?php echo $scheck['name'];?>"/> <input name="id" autofocus="this" type="hidden" value="<?php echo $scheck['id'];?>"/></td></tr>
                      
                            <tr><td>
                                    <input type="checkbox"<?php if($scheck['onechoose'] == TRUE) { echo "checked"; } ?> name="onechoose" name="onechoose" value="BITM"/>&nbsp;BITM</td></tr>
                            <?php 
                            endforeach;
                            ?>
                            <tr><td><button class="sbmtbtn" type="submit">Update</button></td></tr>
                        </table>
                    </form>
                </center>

            </div>
            
            <div class="footer">
                <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
            </div>
        </div>   
</body>
</html>
