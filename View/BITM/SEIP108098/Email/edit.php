<?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../../../../".$filename.".php");
}

use src\BITM\SEIP108098\Email\Email;
$id=$_GET['id'];
$email=new Email();
$email=$email->Select($id);

//print_r($book);
?>
<html>
    <head>
        <title>Edit Email</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../../css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="main_continar_email_edit">
            <div class="email_edit_title"><h1>EMAIL LIST</h1></div> 
            <div class="email_edit_nev">
                <a href="../../../../index108098.php"><strong>HOME</strong></a>&nbsp;&nbsp;
                 <a href="../../../../about.php"><strong>ABOUT</strong></a>&nbsp;&nbsp;
                <a href="index.php"><strong>VIEW</strong></a>
            </div> 
            
            <div class="email_edit_body">
                <center>
                    <form action="update.php" method="post">  
                        <table class="email_edit_table">
                            <?php
                            foreach($email as $email):
                            ?>
                            <tr><td>Edit Email</td></tr>
                                <tr><td><input name="name" size="45" autofocus="this" value="<?php echo $email['name'];?>"/> <input name="id" autofocus="this" type="hidden" value="<?php echo $email['id'];?>"/></td></tr>
                                
                                <tr><td>Email ID</td></tr>
                                <tr><td><input name="email_id" size="45" value="<?php echo $email['email_id'];?>"></td></tr>
                            <?php 
                            endforeach;
                            ?>
                                <tr><td><button class="sbmtbtn" type="submit">Update</button></td></tr>
                        </table>
                    </form>
                </center>
            </div>
            
            <div class="footer">
                <p>  Md.Obaidul Hoque | SEIP 108098 | PHP-13</p>
            </div>
        </div>   
</body>
</html>
